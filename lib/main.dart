import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('This is Text'),
        ), //AppBar
        body: Center(
          child: Text(''' 
          Hello World \n
          My name is Asma Ahmed \n
          I graduate in Spring 2021 \n
          Favorite quote: Catch you on the flippity flip'''),
        ),
      ),
    );
  }
}
